// DataInputWrapper
package puzzle2048;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.IOException;

public final class DataInputWrapper
{
	private DataInputStream dis  = null;
	private InputStream istm = null;
	private boolean error = false;

	public DataInputWrapper(byte[] buf)
	{
		if (buf != null)
		{
			istm = new ByteArrayInputStream(buf);
			dis = new DataInputStream(istm);
		}
	}

	public void close()
	{
		if (dis != null)
		{
			try
			{
				dis.close();
			}
			catch (IOException _)
			{
				// no code
			}
			dis = null;
		}
		if (istm != null) // almost called istm.close() in dis.close()
		{
			try
			{
				istm.close();
			}
			catch (IOException _)
			{
				// no code
			}
			istm = null;
		}
	}

	public int readInt()
	{
		if (dis == null)
		{
			return 0;
		}
		try
		{
			return dis.readInt();
		}
		catch (IOException _)
		{
			error = true;
		}
		return 0;
	}

	public long readLong()
	{
		if (dis == null)
		{
			return 0;
		}
		try
		{
			return dis.readLong();
		}
		catch (IOException _)
		{
			error = true;
		}
		return 0L;
	}

	public boolean readBoolean()
	{
		if (dis == null)
		{
			return false;
		}
		try
		{
			return dis.readBoolean();
		}
		catch (IOException _)
		{
			error = true;
		}
		return false;
	}

	public boolean hasError()
	{
		return error;
	}
}