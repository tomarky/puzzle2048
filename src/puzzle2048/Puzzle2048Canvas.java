// Puzzle2048Canvas
package puzzle2048;

import java.util.Date;
import java.util.Random;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.game.GameCanvas;

import puzzle2048.GameState;
import puzzle2048.Puzzle2048Color;
import puzzle2048.Puzzle2048RMS;
import puzzle2048.WaitController;

public final class Puzzle2048Canvas extends GameCanvas implements Runnable
{
	// private static final int DISPLAY_WIDTH  = 240;
	// private static final int DISPLAY_HEIGTH = 268;
	
	private static final int NO_BACKUPSTATE   = -1;
	
	private static final int STATE_WAIT_KEY     = 0;
	private static final int STATE_NEW_GAME     = 1;
	private static final int STATE_NEW_TILE     = 2;
	private static final int STATE_GAMEOVER     = 3;
	private static final int STATE_HELPVIEW     = 4;
	private static final int STATE_WINNER       = 5;
	private static final int STATE_RESETSCORE   = 6;
	private static final int STATE_SHOWSCORES   = 7;
	private static final int STATE_SHOWCAPTURES = 8;
	
	private static final int FLAG_NONE              = 0x00;
	private static final int FLAG_REQ_NEW_GAME      = 0x01;
	private static final int FLAG_REQ_END_GAME      = 0x02;
	private static final int FLAG_REQ_SHOW_HELP     = 0x04;
	private static final int FLAG_REQ_RESET_SCORE   = 0x08;
	private static final int FLAG_REQ_SHOW_SCORES   = 0x10;
	private static final int FLAG_REQ_SHOW_CAPTURES = 0x20;
	
	private static final int TRANS_UP    = 0;
	private static final int TRANS_LEFT  = 1;
	private static final int TRANS_DOWN  = 2;
	private static final int TRANS_RIGHT = 3;
	
	private int  score     = 0;
	private int  best      = 0;
	private long scoreTime = 0L;
	private boolean winner = false;
	
	private Puzzle2048RMS rms            = null;
	private Image         baseFieldImage = null;
	private Image         shineImage     = null;
	
	private int state        = STATE_NEW_GAME;
	private int flagValue    = FLAG_NONE;
	private int backupState  = NO_BACKUPSTATE;
	private int captureIndex = 0;

	private final int CELL_WIDTH;
	private final int CELL_HEIGTH;
	private final int WIDTH;
	private final int HEIGTH;
	
	private final int[][]  field;	
	private final int[][]  tempField;
	private final int[][]  tileMoveXs;
	private final int[][]  tileMoveYs;
	private final String[] tileNumberTexts;
	private final Image[]  tileImages;
	
	private final int[]     highScores;
	private final long[]    highScoreDates;
	private final int[][][] captures;
	
	private final WaitController waitController;
	private final Random rand;
	private final Font   largeFont;
	private final Font   mediumFont;
	private final Font   smallFont;
	
	public Puzzle2048Canvas(int w, int h)
	{
		super(false);
		WIDTH       = w;
		HEIGTH      = h;
		CELL_WIDTH  = 240 / w;
		CELL_HEIGTH = 240 / h;
		field       = new int[h][w];
		tempField   = new int[h][w];
		tileMoveXs  = new int[h][w];
		tileMoveYs  = new int[h][w];
		rand        = new Random();
		largeFont   = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_LARGE);
		mediumFont  = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_MEDIUM);
		smallFont   = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_SMALL);
		
		highScores = new int[10];
		highScoreDates = new long[highScores.length];
		captures = new int[highScores.length][h][w];
		
		tileImages         = new Image[20];
		tileNumberTexts    = new String[20];
		tileNumberTexts[0] = "";
		for (int i = 1; i < tileNumberTexts.length; i++)
		{
			tileNumberTexts[i] = Integer.toString(1 << i);
		}
		waitController = new WaitController();
	}
	
	private boolean enablesFlag()
	{
		int tmpFlagValue = flagValue;
		return tmpFlagValue != FLAG_NONE;
	}
	
	private boolean enablesFlag(int flag)
	{
		int tmpFlagValue = flagValue;
		return (tmpFlagValue & flag) == flag;
	}
	
	private void resetFlag()
	{
		flagValue = FLAG_NONE;
	}
	
	private void switchFlag(int flag)
	{
		int tmpFlagValue = flagValue;
		tmpFlagValue ^= flag;
		flagValue = tmpFlagValue;
	}
	
	private void switchFlag(int flag, boolean enable)
	{
		int tmpFlagValue = flagValue;
		if (enable)
		{
			tmpFlagValue |= flag;
		}
		else
		{
			tmpFlagValue &= ~flag;
		}
		flagValue = tmpFlagValue;
	}
	
	private int pack(int x, int y, int v)
	{
		return (v * HEIGTH + y) * WIDTH + x;
	}
	
	private int unpackX(int p)
	{
		return p % WIDTH;
	}
	
	private int unpackY(int p)
	{
		return (p / WIDTH) % HEIGTH;
	}
	
	private int unpackV(int p)
	{
		return p / WIDTH / HEIGTH;
	}
	
	private int initGame()
	{
		score = 0;
		scoreTime = 0L;
		winner = false;
		for (int y = 0; y < HEIGTH; y++)
		{
			for (int x = 0; x < WIDTH; x++)
			{
				field[y][x] = 0;
			}
		}
		int px = rand.nextInt(WIDTH);
		int py = rand.nextInt(HEIGTH);
		field[py][px] = 1;
		return pack(px, py, field[py][px]);
	}
	
	private void animateNewTiles(Graphics g)
	{
		animateNewTiles(g, -1, -1);
	}
	
	private void animateNewTiles(Graphics g, int px, int py)
	{
		waitController.init(50L);
		int motions = 2;
		for (int i = 0; i < motions; i++)
		{
			int dw = CELL_WIDTH  * (i + 1) / (motions + 1);
			int dh = CELL_HEIGTH * (i + 1) / (motions + 1);
			
			for (int y = 0; y < HEIGTH; y++)
			{
				for (int x = 0; x < WIDTH; x++)
				{
					if (field[y][x] == 0)
					{
						continue;
					}
					if (px >= 0 && (px != x || py != y))
					{
						continue;
					}
					int cx = x * CELL_WIDTH; 
					int cy = y * CELL_HEIGTH;
					g.setColor(Puzzle2048Color.getTileColor(field[y][x]));
					g.fillRoundRect(cx + (CELL_WIDTH  - dw) / motions,
							cy + (CELL_HEIGTH - dh) / motions, dw, dh, 2, 2);
					if (i > 0)
					{
						drawTileNumber(g, field[y][x], cx, cy);
					}
				}
			}
			
			flushGraphics();
			
			waitController.await();
		}
	}
	
	private void newGame(Graphics g)
	{
		int p1 = initGame();
		int x1 = unpackX(p1);
		int y1 = unpackY(p1);
		int v1 = unpackV(p1);
		
		int p2 = nextTile();
		int x2 = unpackX(p2);
		int y2 = unpackY(p2);
		int v2 = unpackV(p2);
		field[y2][x2] = v2;
		
		drawScoreBoard(g);
		drawBaseField(g);		
		flushGraphics();
		
		animateNewTiles(g);
		
		drawTiles(g);
		flushGraphics();
		
		setNewState(STATE_WAIT_KEY);
	}
	
	private int countEmptyCells()
	{
		int rem = 0;
		for (int y = 0; y < HEIGTH; y++)
		{
			for (int x = 0; x < WIDTH; x++)
			{
				if (field[y][x] == 0)
				{
					rem++;
				}
			}
		}
		return rem;
	}
	
	private int nextTile()
	{
		int pos;
		{
			int rem = countEmptyCells();
			if (rem == 0)
			{
				throw new RuntimeException("BUG!!");
			}
			pos = rand.nextInt(rem);
		}
		int val = rand.nextInt(100) < 90 ? 1 : 2;
		for (int y = 0; y < HEIGTH; y++)
		{
			for (int x = 0; x < WIDTH; x++)
			{
				if (field[y][x] != 0)
				{
					continue;
				}
				if (pos == 0)
				{
					return pack(x, y, val);
				}
				pos--;
			}
		}
		throw new RuntimeException("BUG!!");
	}
	
	private boolean isGameOver()
	{
		if (countEmptyCells() > 0)
		{
			return false;
		}
		for (int y = 0; y < HEIGTH; y++)
		{
			for (int x = 0; x < WIDTH; x++)
			{
				if (x > 0 && field[y][x] == field[y][x-1])
				{
					return false;
				}
				if (y > 0 && field[y][x] == field[y-1][x])
				{
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean isWinner()
	{
		for (int y = 0; y < HEIGTH; y++)
		{
			for (int x = 0; x < WIDTH; x++)
			{
				if (field[y][x] == 11) /* (1 << 11) == 2048 */
				{
					return true;
				}
			}
		}
		return false;
	}
	
	private void newTile(Graphics g)
	{
		drawScoreBoard(g);
		drawBaseField(g);
		drawTiles(g);
		flushGraphics();

		int p = nextTile();
		int x = unpackX(p);
		int y = unpackY(p);
		int v = unpackV(p);

		field[y][x] = v;	
		
		animateNewTiles(g, x, y);
		
		drawTiles(g);
		flushGraphics();
		
		if (winner == false && isWinner())
		{
			waitController.await(50L);
			drawWinner(g);
			winner = true;
			setNewState(STATE_WINNER);
			return;
		}
		
		setNewState(isGameOver() ? STATE_GAMEOVER : STATE_WAIT_KEY);
	}
	
	private void drawScoreBoard(Graphics g)
	{
		g.setColor(Puzzle2048Color.BACK);
		g.fillRect(0, 240, 240, 268-240);
		
		g.setColor(Puzzle2048Color.SCOREBOARD);
		g.fillRoundRect(  0+2, 240+2, 120-4, (268-240)-4, 7, 7);
		g.fillRoundRect(120+2, 240+2, 120-4, (268-240)-4, 7, 7);
		
		g.setFont(smallFont);
		g.setColor(Puzzle2048Color.SCOREBOARD_FONT);
		g.drawString("SCORE",   0+4, 240+8, Graphics.TOP | Graphics.LEFT);
		g.drawString("BEST",  120+4, 240+8, Graphics.TOP | Graphics.LEFT);
		
		g.setFont(largeFont);
		g.setColor(Puzzle2048Color.SCORE_FONT);
		g.drawString(Integer.toString(score), 120-4, 240+4, Graphics.TOP | Graphics.RIGHT);
		g.drawString(Integer.toString(best),  240-4, 240+4, Graphics.TOP | Graphics.RIGHT);
	}
	
	private void drawBaseField(Graphics g)
	{
		g.drawImage(baseFieldImage, 0, 0, Graphics.TOP | Graphics.LEFT);
	}
	
	private void initBaseFieldImage()
	{
		baseFieldImage = Image.createImage(240, 240);
		Graphics g = baseFieldImage.getGraphics();
		g.setColor(Puzzle2048Color.FIELD);
		g.fillRect(0, 0, 240, 240);
		g.setColor(Puzzle2048Color.getTileColor(0));
		for (int y = 0; y < HEIGTH; y++)
		{
			int cy = y * CELL_HEIGTH;
			for (int x = 0; x < WIDTH; x++)
			{
				int cx = x * CELL_WIDTH;
				drawTile(g, 0, cx, cy);
			}
		}
	}
	
	private void initShineImage()
	{
		Image tempImage = Image.createImage(CELL_WIDTH, CELL_HEIGTH);
		Graphics g = tempImage.getGraphics();
		g.setColor(0);
		g.fillRect(0, 0, CELL_WIDTH, CELL_HEIGTH);
		int color = Puzzle2048Color.FIELD; 
		int red   = Math.min(0xFF, ((color >> 16) & 0xFF) + 0x15);
		int green = Math.min(0xFF, ((color >>  8) & 0xFF) + 0x15);
		int blue  = color & 0xFF;
		g.setColor(red, green, blue);
		g.fillRoundRect(0, 0, CELL_WIDTH, CELL_HEIGTH, 7, 7);
		red   = Math.min(0xFF, ((color >> 16) & 0xFF) + 0x20);
		green = Math.min(0xFF, ((color >>  8) & 0xFF) + 0x20);
		g.setColor(red, green, blue);
		g.fillRoundRect(1, 1, CELL_WIDTH - 2, CELL_HEIGTH - 2, 7, 7);
		int[] buf = new int[CELL_WIDTH * CELL_HEIGTH];
		tempImage.getRGB(buf, 0, CELL_WIDTH, 0, 0, CELL_WIDTH, CELL_HEIGTH);
		for (int p = 0; p < buf.length; p++)
		{
			buf[p] &= 0xFFFFFF;
			if (buf[p] != 0)
			{
				buf[p] |= 0xFF000000;
			}
		}
		shineImage = Image.createRGBImage(buf, CELL_WIDTH, CELL_HEIGTH, true);
	}
	
	private void drawTileShape(Graphics g, int x, int y)
	{
		g.fillRoundRect(x + 2, y + 2, CELL_WIDTH - 4, CELL_HEIGTH - 4, 7, 7);
	}
	
	private void initTileImages()
	{
		Image tempImage = Image.createImage(CELL_WIDTH, CELL_HEIGTH);
		Graphics g = tempImage.getGraphics();
		g.setColor(0);
		g.fillRect(0, 0, CELL_WIDTH, CELL_HEIGTH);
		for (int i = 0; i < tileImages.length; i++)
		{
			g.setColor(Puzzle2048Color.getTileColor(i));
			drawTileShape(g, 0, 0);
			if (i > 0)
			{
				drawTileNumber(g, i, 0, 0);
			}
			int[] buf = new int[CELL_WIDTH * CELL_HEIGTH];
			tempImage.getRGB(buf, 0, CELL_WIDTH, 0, 0, CELL_WIDTH, CELL_HEIGTH);
			for (int p = 0; p < buf.length; p++)
			{
				buf[p] &= 0xFFFFFF;
				if (buf[p] != 0)
				{
					buf[p] |= 0xFF000000;
				}
			}
			tileImages[i] = Image.createRGBImage(buf, CELL_WIDTH, CELL_HEIGTH, true);
		}
	}
	
	private void drawTile(Graphics g, int tile, int x, int y)
	{
		g.drawImage(tileImages[tile], x, y, Graphics.TOP | Graphics.LEFT);
	}

	private void drawLargeTile(Graphics g, int tile, int x, int y)
	{
		g.setColor(Puzzle2048Color.getTileColor(tile));
		g.fillRoundRect(x, y, CELL_WIDTH, CELL_HEIGTH, 7, 7);
		if (tile > 0)
		{
			drawTileNumber(g, tile, x, y);
		}
	}
	
	private void drawTileNumber(Graphics g, int tile, int x, int y)
	{
		String tileStr = tileNumberTexts[tile];
		Font   selFont = tileStr.length() < 6 ? largeFont : mediumFont;
		int fontWidth = selFont.stringWidth(tileStr);
		int marginX   = (CELL_WIDTH - fontWidth) / 2;
		int marginY   = (CELL_HEIGTH - selFont.getHeight()) / 2;
		g.setFont(selFont);
		g.setColor(Puzzle2048Color.getTileFontColor(tile));
		g.drawString(tileStr, x + marginX, y + marginY, Graphics.TOP | Graphics.LEFT);
	}
	
	private int transX(int trans, int p)
	{
		switch (trans)
		{
		case TRANS_UP:    return p / HEIGTH;
		case TRANS_LEFT:  return p % WIDTH;
		case TRANS_DOWN:  return p / HEIGTH;
		case TRANS_RIGHT: return (WIDTH - 1) - p % WIDTH;
		}
		throw new RuntimeException("BUG!!");
	}
	
	private int transY(int trans, int p)
	{
		switch (trans)
		{
		case TRANS_UP:    return p % HEIGTH;
		case TRANS_LEFT:  return p / WIDTH;
		case TRANS_DOWN:  return (HEIGTH - 1) -p % HEIGTH;
		case TRANS_RIGHT: return p / WIDTH;
		}
		throw new RuntimeException("BUG!!");
	}
	
	private int transInitXp(int trans, int p, int xp)
	{
		switch (trans)
		{
		case TRANS_UP:    return p / HEIGTH;
		case TRANS_LEFT:  return (p % WIDTH == 0) ? 0 : xp;
		case TRANS_DOWN:  return p / HEIGTH;
		case TRANS_RIGHT: return (p % WIDTH == 0) ? WIDTH - 1 : xp;
		}
		throw new RuntimeException("BUG!!");
	}
	
	private int transInitYp(int trans, int p, int yp)
	{
		switch (trans)
		{
		case TRANS_UP:    return (p % HEIGTH == 0) ? 0 : yp;
		case TRANS_LEFT:  return p / WIDTH;
		case TRANS_DOWN:  return (p % HEIGTH == 0) ? HEIGTH - 1 : yp;
		case TRANS_RIGHT: return p / WIDTH;
		}
		throw new RuntimeException("BUG!!");
	}
	
	private int transNextXp(int trans, int xp)
	{
		switch (trans)
		{
		case TRANS_UP:    return xp;
		case TRANS_LEFT:  return xp + 1;
		case TRANS_DOWN:  return xp;
		case TRANS_RIGHT: return xp - 1;
		}
		throw new RuntimeException("BUG!!");
	}
	
	private int transNextYp(int trans, int yp)
	{
		switch (trans)
		{
		case TRANS_UP:    return yp + 1;
		case TRANS_LEFT:  return yp;
		case TRANS_DOWN:  return yp - 1;
		case TRANS_RIGHT: return yp;
		}
		throw new RuntimeException("BUG!!");
	}
	
	private void initCalcMoveTiles()
	{
		for (int y = 0; y < HEIGTH; y++)
		{
			for (int x = 0; x < WIDTH; x++)
			{
				tempField[y][x] = 0;
				tileMoveXs[y][x] = x;
				tileMoveYs[y][x] = y;
			}
		}		
	}
	
	private int calcMoveTiles(int trans)
	{
		int xp = 0, yp = 0, plusscore = 0;
		for (int p = 0; p < WIDTH * HEIGTH; p++)
		{
			int x = transX(trans, p);
			int y = transY(trans, p);
			xp = transInitXp(trans, p, xp);
			yp = transInitYp(trans, p, yp);
			
			if (field[y][x] == 0)
			{
				continue;
			}
			
			if (tempField[yp][xp] == 0)
			{
				tempField[yp][xp] = field[y][x];
				tileMoveXs[y][x] = xp;
				tileMoveYs[y][x] = yp;
			}
			else if (tempField[yp][xp] == field[y][x])
			{
				plusscore += 2 << field[y][x];
				tempField[yp][xp] = -1 - field[y][x];
				tileMoveXs[y][x] = xp;
				tileMoveYs[y][x] = yp;
				xp = transNextXp(trans, xp);
				yp = transNextYp(trans, yp);
			}
			else
			{
				xp = transNextXp(trans, xp);
				yp = transNextYp(trans, yp);
				tempField[yp][xp] = field[y][x];
				tileMoveXs[y][x] = xp;
				tileMoveYs[y][x] = yp;
			}
		}
		return plusscore;
	}
	
	private boolean isTilesMoved()
	{
		for (int y = 0; y < HEIGTH; y++)
		{
			for (int x = 0; x < WIDTH; x++)
			{
				if (tempField[y][x] != field[y][x])
				{
					return true;
				}
			}
		}
		return false;
	}
	
	private void animateMoveTiles(Graphics g, int trans)
	{
		// animation
		waitController.init(50L);
		int motions = 1;
		for (int i = 0; i < motions; i++)
		{
			drawBaseField(g);
			for (int p = 0; p < WIDTH * HEIGTH; p++)
			{
				int x = transX(trans, p);
				int y = transY(trans, p);
				if (field[y][x] == 0)
				{
					continue;
				}
				int fy = y * CELL_HEIGTH;
				int fx = x * CELL_WIDTH;
				int tx = tileMoveXs[y][x] * CELL_WIDTH;
				int ty = tileMoveYs[y][x] * CELL_HEIGTH;
					
				int cx = ((motions + 1) * fx + (tx - fx) * (i + 1)) / (motions + 1);
				int cy = ((motions + 1) * fy + (ty - fy) * (i + 1)) / (motions + 1);
				
				drawTile(g, field[y][x], cx, cy);
			}
			flushGraphics();
			
			waitController.await();
		}
	}
	
	private void mergeTiles(Graphics g, int plusscore)
	{
		waitController.init(50L);
		// merge
		if (plusscore > 0)
		{
			score += plusscore;
			if (score > best)
			{
				best = score;
			}
			drawScoreBoard(g);
		}
		drawBaseField(g);
		for (int y = 0; y < HEIGTH; y++)
		{
			int cy = y * CELL_HEIGTH;
			for (int x = 0; x < WIDTH; x++)
			{
				int t = tempField[y][x];
				int cx = x * CELL_WIDTH;
				if (t < 0)
				{
					t = -t;
					drawLargeTile(g, t, cx, cy);
				}
				else if (t > 0)
				{
					drawTile(g, t, cx, cy);
				}
				field[y][x] = t;
			}
		}
		flushGraphics();
		
		waitController.await();

	}
	
	private void moveTiles(Graphics g, int trans)
	{
		initCalcMoveTiles();
		int plusscore = calcMoveTiles(trans);
		if (isTilesMoved() == false)
		{
			return;
		}
		
		animateMoveTiles(g, trans);
		
		mergeTiles(g, plusscore);
		
		drawBaseField(g);
		drawTiles(g);
		flushGraphics();

		scoreTime = System.currentTimeMillis();
		setNewState(STATE_NEW_TILE);
	}
	
	private void drawTiles(Graphics g)
	{
		for (int y = 0; y < HEIGTH; y++)
		{
			int cy = y * CELL_HEIGTH;
			for (int x = 0; x < WIDTH; x++)
			{
				int tile = field[y][x];
				if (tile == 0)
				{
					continue;
				}
				int cx = x * CELL_WIDTH;
				if (tempField[y][x] < -8)
				{
					g.drawImage(shineImage, cx, cy, Graphics.TOP | Graphics.LEFT);
				}
				drawTile(g, tile, cx, cy);
			}
		}
	}
	
	private void drawWinner(Graphics g)
	{
		for (int y = 0; y < HEIGTH; y++)
		{
			int cy = y * CELL_HEIGTH;
			for (int x = 0; x < WIDTH; x++)
			{
				int cx = x * CELL_WIDTH;
				int color = Puzzle2048Color.getTileColor(field[y][x]);
				int red   = Math.min(0xFF, ((color >> 16) & 0xFF) + 0x10);
				int green = Math.min(0xFF, ((color >>  8) & 0xFF) + 0x10);
				int blue  = color & 0xFF;
				if (tempField[y][x] < -8)
				{
					g.drawImage(shineImage, cx, cy, Graphics.TOP | Graphics.LEFT);
				}
				g.setColor(red, green, blue);
				drawTileShape(g, cx, cy);
				drawTileNumber(g, field[y][x], cx, cy);
			}
		}
		g.setFont(largeFont);
		String msg = "You Win";
		int sw = largeFont.stringWidth(msg);
		int x = 120 - sw / 2;
		int y = 120 - largeFont.getHeight() / 2;
		g.setColor(0x000000);
		for (int dy = -1; dy <= 1; dy++)
		{
			for (int dx = -1; dx <= 1; dx++)
			{
				g.drawString(msg, x + dx, y + dy, Graphics.TOP | Graphics.LEFT);
			}
		}
		g.setColor(0xFFFFFF);
		g.drawString(msg, x, y, Graphics.TOP | Graphics.LEFT);
		
		g.setFont(smallFont);
		msg = "press " + getKeyName(FIRE) + " to continue";
		sw = smallFont.stringWidth(msg);
		x = 120 - sw / 2;
		y = 180 - smallFont.getHeight() / 2;
		g.setColor(0x000000);
		g.drawString(msg, x, y, Graphics.TOP | Graphics.LEFT);
		
		flushGraphics();
	}
	
	private void drawGameOver(Graphics g)
	{
		for (int y = 0; y < HEIGTH; y++)
		{
			int cy = y * CELL_HEIGTH;
			for (int x = 0; x < WIDTH; x++)
			{
				int cx = x * CELL_WIDTH;
				int color = Puzzle2048Color.getTileColor(field[y][x]);
				int red   = Math.max(0, ((color >> 16) & 0xFF) - 0x40);
				int green = Math.max(0, ((color >>  8) & 0xFF) - 0x40);
				int blue  = Math.max(0, ( color        & 0xFF) - 0x40);
				g.setColor(red, green, blue);
				drawTileShape(g, cx, cy);
				drawTileNumber(g, field[y][x], cx, cy);
			}
		}
		g.setFont(largeFont);
		String msg = "GameOver";
		int sw = largeFont.stringWidth(msg);
		int x = 120 - sw / 2;
		int y = 120 - largeFont.getHeight() / 2;
		g.setColor(0x000000);
		for (int dy = -1; dy <= 1; dy++)
		{
			for (int dx = -1; dx <= 1; dx++)
			{
				g.drawString(msg, x + dx, y + dy, Graphics.TOP | Graphics.LEFT);
			}
		}
		g.setColor(0xFFFFFF);
		g.drawString(msg, x, y, Graphics.TOP | Graphics.LEFT);
		flushGraphics();
	}
	
	private void loadGameData(Graphics g)
	{
		if (rms == null)
		{
			rms = new Puzzle2048RMS();
		}
		
		best = rms.loadBestScore();
		rms.loadHighScores(highScores, highScoreDates);
		rms.loadCaptures(WIDTH, HEIGTH, captures);

		GameState gamestate = rms.loadGameState(field, WIDTH, HEIGTH);
		if (gamestate != null && gamestate.field == field)
		{
			state = gamestate.state;
			score = gamestate.score;
			scoreTime = gamestate.scoreTime;
			winner = gamestate.winner;
			drawScoreBoard(g);
			drawBaseField(g);
			if (state == STATE_WINNER)
			{
				drawWinner(g);
			}
			else if (isGameOver())
			{
				drawGameOver(g);
			}
			else
			{
				flushGraphics();
				animateNewTiles(g);
				drawTiles(g);
			}
			flushGraphics();
		}
	}
	
	private void saveGameData()
	{
		if (rms == null)
		{
			rms = new Puzzle2048RMS();
		}
		rms.saveBestScore(best);
		rms.saveHighScores(highScores, highScoreDates);
		rms.saveCaptures(WIDTH, HEIGTH, captures);
		GameState gamestate = new GameState();
		gamestate.state     = state;
		gamestate.score     = score;
		gamestate.scoreTime = scoreTime;
		gamestate.winner    = winner;
		gamestate.width     = WIDTH;
		gamestate.height    = HEIGTH;
		gamestate.field     = field;
		rms.saveGameState(gamestate);
		rms.close();
		rms = null;
	}

	private void confirmResetScore(Graphics g)
	{
		g.setColor(Puzzle2048Color.BACK);
		g.fillRect(20, 20, 240-40, 240-40);
		g.setColor(Puzzle2048Color.SCOREBOARD);
		g.drawRect(20, 20, 240-40, 240-40);
		
		g.setFont(smallFont);
		int dh = smallFont.getHeight();
		
		String msg = "reset best and high scores ?";
		g.drawString(msg, 30, 30, Graphics.TOP | Graphics.LEFT);
		
		g.drawString("reset:",  80, 30+dh*2, Graphics.TOP | Graphics.RIGHT);
		g.drawString("cancel:", 80, 30+dh*4, Graphics.TOP | Graphics.RIGHT);
		
		g.drawString(getKeyName(GAME_C), 85, 30+dh*2,   Graphics.TOP | Graphics.LEFT);
		g.drawString(getKeyName(GAME_D), 85, 30+dh*4, Graphics.TOP | Graphics.LEFT);
		
		for (int i = 2; i <= 4; i += 2)
		{
			g.drawString("or", 120, 30+dh*i, Graphics.TOP | Graphics.LEFT);
		}
		
		g.drawString(getKeyName(KEY_NUM7), 140, 30+dh*2, Graphics.TOP | Graphics.LEFT);
		g.drawString(getKeyName(KEY_NUM9), 140, 30+dh*4, Graphics.TOP | Graphics.LEFT);
		
		flushGraphics();
	}
	
	private void showHelp(Graphics g)
	{
		g.setColor(Puzzle2048Color.BACK);
		g.fillRect(20, 20, 240-40, 240-40);
		g.setColor(Puzzle2048Color.SCOREBOARD);
		g.drawRect(20, 20, 240-40, 240-40);
		
		g.setFont(smallFont);
		int dh = smallFont.getHeight();
		
		g.drawString("move tiles:", 30, 30, Graphics.TOP | Graphics.LEFT);
		g.drawString("up:",    70, 30+dh*2,  Graphics.TOP | Graphics.RIGHT);
		g.drawString("down:",  70, 30+dh*4, Graphics.TOP | Graphics.RIGHT);
		g.drawString("left:",  70, 30+dh*6, Graphics.TOP | Graphics.RIGHT);
		g.drawString("right:", 70, 30+dh*8, Graphics.TOP | Graphics.RIGHT);
		
		g.drawString(getKeyName(UP),    75, 30+dh*2, Graphics.TOP | Graphics.LEFT);
		g.drawString(getKeyName(DOWN),  75, 30+dh*4, Graphics.TOP | Graphics.LEFT);
		g.drawString(getKeyName(LEFT),  75, 30+dh*6, Graphics.TOP | Graphics.LEFT);
		g.drawString(getKeyName(RIGHT), 75, 30+dh*8, Graphics.TOP | Graphics.LEFT);
		
		for (int i = 2; i <= 8; i += 2)
		{
			g.drawString("or", 110, 30+dh*i, Graphics.TOP | Graphics.LEFT);
		}
		
		g.drawString(getKeyName(KEY_NUM2), 130, 30+dh*2, Graphics.TOP | Graphics.LEFT);
		g.drawString(getKeyName(KEY_NUM8), 130, 30+dh*4, Graphics.TOP | Graphics.LEFT);
		g.drawString(getKeyName(KEY_NUM4), 130, 30+dh*6, Graphics.TOP | Graphics.LEFT);
		g.drawString(getKeyName(KEY_NUM6), 130, 30+dh*8, Graphics.TOP | Graphics.LEFT);
		
		flushGraphics();
	}
	
	private String formatDateString(Date dt)
	{
		String s = dt.toString();
		return s.substring(s.length() - 4) + " " + s.substring(4, 19);
	}

	private void showScores(Graphics g)
	{
		g.setColor(Puzzle2048Color.BACK);
		g.fillRect(20, 20, 240-40, 240-40);
		g.setColor(Puzzle2048Color.SCOREBOARD);
		g.drawRect(20, 20, 240-40, 240-40);
		
		g.setFont(smallFont);
		int dh = smallFont.getHeight();
		Date dt = new Date();
		int off = 0;
		
		g.drawString("High Scores:", 30, 30, Graphics.TOP | Graphics.LEFT);
		for (int i = 0; i < highScores.length; i++)
		{
			String rank  = Integer.toString(i + 1) + ":";
			String scstr = Integer.toString(highScores[i + off]);
			dt.setTime(highScoreDates[i + off]);
			if (off == 0 && score > highScores[i])
			{
				scstr = Integer.toString(score);
				dt.setTime(scoreTime);
				off = -1;
				g.setColor(0xFF0000);
			}
			else
			{
				g.setColor(Puzzle2048Color.SCOREBOARD);
			}
			String dtstr = formatDateString(dt);
			int y = dh * (i + 2) + 30;
			g.drawString(rank,   40, y, Graphics.TOP | Graphics.RIGHT);
			g.drawString(scstr,  90, y, Graphics.TOP | Graphics.RIGHT);
			g.drawString(dtstr, 100, y, Graphics.TOP | Graphics.LEFT);
		}
		
		flushGraphics();
	}
	
	private String getOrdName(int r)
	{
		int rem = r % 10;
		if (r == 1 && r != 11)
		{
			return Integer.toString(r) + "st";
		}
		if (r == 2 && r != 12)
		{
			return Integer.toString(r) + "nd";
		}
		if (r == 3 && r != 13)
		{
			return Integer.toString(r) + "rd";
		}
		return Integer.toString(r) + "th";
	}
	
	private void showCapture(Graphics g)
	{
		int r = -1;
		for (int i = 0; i < highScores.length; i++)
		{
			if (score > highScores[i])
			{
				r = i;
				break;
			}
		}
		int[][] f;
		int sc;
		if (r < 0 || captureIndex < r)
		{
			f = captures[captureIndex];
			sc = highScores[captureIndex];
		}
		else if (captureIndex > r)
		{
			f = captures[captureIndex - 1];
			sc = highScores[captureIndex - 1];
		}
		else
		{
			f = field;
			sc = score;
		}
		drawBaseField(g);
		for (int y = 0; y < HEIGTH; y++)
		{
			int cy = y * CELL_HEIGTH;
			for (int x = 0; x < WIDTH; x++)
			{
				int cx = x * CELL_WIDTH;
				int color = Puzzle2048Color.getTileColor(f[y][x]);
				int red   = Math.max(0, ((color >> 16) & 0xFF) - 0x40);
				int green = Math.max(0, ((color >>  8) & 0xFF) - 0x40);
				int blue  = Math.max(0, ( color        & 0xFF) - 0x40);
				g.setColor(red, green, blue);
				drawTileShape(g, cx, cy);
				drawTileNumber(g, f[y][x], cx, cy);
			}
		}
		g.setFont(largeFont);
		String msg = getOrdName(captureIndex + 1) + " SCORE: " + Integer.toString(sc);
		int sw = largeFont.stringWidth(msg);
		int x = 120 - sw / 2;
		int y = 120 - largeFont.getHeight() / 2;
		g.setColor(0x000000);
		for (int dy = -1; dy <= 1; dy++)
		{
			for (int dx = -1; dx <= 1; dx++)
			{
				g.drawString(msg, x + dx, y + dy, Graphics.TOP | Graphics.LEFT);
			}
		}
		g.setColor(r == captureIndex ? 0xFFFF00 : 0xFFFFFF);
		g.drawString(msg, x, y, Graphics.TOP | Graphics.LEFT);
		flushGraphics();
	}
	
	private void redrawAll(Graphics g)
	{
		drawScoreBoard(g);
		drawBaseField(g);
		if (state == STATE_WINNER)
		{
			drawWinner(g);
		}
		else if (isGameOver())
		{
			drawGameOver(g);
		}
		else
		{
			drawTiles(g);
		}
		flushGraphics();
	}
	
	private boolean hasBackupState()
	{
		return backupState != NO_BACKUPSTATE;
	}
	
	private void setNewState(int newState)
	{
		restoreState();
		state = newState;
	}
	
	private boolean setBackupState(int newState, boolean force)
	{
		if (hasBackupState())
		{
			if (force == false)
			{
				return false;
			}
		}
		else
		{
			backupState = state;
		}
		state = newState;
		return true;
	}
	
	private void restoreState()
	{
		if (hasBackupState())
		{
			state = backupState;
			backupState = NO_BACKUPSTATE;
		}
	}
	
	private void restoreAll(Graphics g)
	{
		restoreState();
		redrawAll(g);
		waitController.await(50L); // reset keyStates?
	}
	
	private boolean doFlagTask(Graphics g)
	{
		if (enablesFlag(FLAG_REQ_END_GAME))
		{
			return true;
		}
		if (enablesFlag(FLAG_REQ_NEW_GAME))
		{
			switchFlag(FLAG_REQ_NEW_GAME, false);
			setNewState(STATE_NEW_GAME);
		}
		else if (enablesFlag(FLAG_REQ_SHOW_HELP))
		{
			switchFlag(FLAG_REQ_SHOW_HELP, false);
			if (state == STATE_HELPVIEW)
			{
				restoreAll(g);
			}
			else if (setBackupState(STATE_HELPVIEW, true))
			{
				showHelp(g);
			}
		}
		else if (enablesFlag(FLAG_REQ_RESET_SCORE))
		{
			switchFlag(FLAG_REQ_RESET_SCORE, false);
			if (state == STATE_RESETSCORE)
			{
				restoreAll(g);
			}
			else if (setBackupState(STATE_RESETSCORE, true))
			{
				confirmResetScore(g);
			}
		}
		else if (enablesFlag(FLAG_REQ_SHOW_SCORES))
		{
			switchFlag(FLAG_REQ_SHOW_SCORES, false);
			if (state == STATE_SHOWSCORES)
			{
				restoreAll(g);
			}
			else if (setBackupState(STATE_SHOWSCORES, true))
			{
				showScores(g);
			}
		}
		else if (enablesFlag(FLAG_REQ_SHOW_CAPTURES))
		{
			switchFlag(FLAG_REQ_SHOW_CAPTURES, false);
			if (state == STATE_SHOWCAPTURES)
			{
				restoreAll(g);
			}
			else if (setBackupState(STATE_SHOWCAPTURES, true))
			{
				captureIndex = 0;
				showCapture(g);
			}
		}
		return false;
	}
	
	public String getKeyName(int keyCode)
	{
		try
		{
			return super.getKeyName(keyCode);
		}
		catch (IllegalArgumentException _)
		{
			// no code
		}
		switch (keyCode)
		{
		case UP:     return "UP";
		case LEFT:   return "LEFT";
		case RIGHT:  return "RIGHT";
		case DOWN:   return "DOWN";
		case FIRE:   return "FIRE";
		case GAME_C: return "SOFT3";
		case GAME_D: return "SOFT4";
		case KEY_POUND: return "#";
		}
		if (KEY_NUM0 <= keyCode && keyCode <= KEY_NUM9)
		{
			return Integer.toString(keyCode - KEY_NUM0);
		}
		return Integer.toString(keyCode);
	}
	
	private void updateHighScores()
	{
		for (int i = 0; i < highScores.length; i++)
		{
			if (score <= highScores[i])
			{
				continue;
			}
			int[][] f = captures[highScores.length - 1];
			for (int j = highScores.length - 1; j > i; j--)
			{
				highScores[j] = highScores[j - 1];
				highScoreDates[j] = highScoreDates[j - 1];
				captures[j] = captures[j - 1];
			}
			captures[i] = f;
			highScores[i] = score;
			highScoreDates[i] = scoreTime;
			for (int y = 0; y < HEIGTH; y++)
			{
				for (int x = 0; x < WIDTH; x++)
				{
					f[y][x] = field[y][x];
				}
			}
			break;
		}
	}
	
	private void clearHiscores()
	{
		best = score;
		for (int i = 0; i < highScores.length; i++)
		{
			highScores[i] = 0;
			highScoreDates[i] = 0L;
			for (int y = 0; y < HEIGTH; y++)
			{
				for (int x = 0; x < WIDTH; x++)
				{
					captures[i][y][x] = 0;
				}
			}
		}
	}
	
	public void run()
	{
		Graphics g = getGraphics();
		
		initShineImage();
		initTileImages();
		initBaseFieldImage();
		
		loadGameData(g);
		
	mainloop:
		for (;;)
		{
			switch (state)
			{
			case STATE_WAIT_KEY:
				if (enablesFlag())
				{
					if (doFlagTask(g))
					{
						break mainloop;
					}
					continue mainloop;
				}
				{
					int keyStates = getKeyStates();
					switch (keyStates)
					{
					case UP_PRESSED:
						moveTiles(g, TRANS_UP);
						break;
					case LEFT_PRESSED:
						moveTiles(g, TRANS_LEFT);
						break;
					case DOWN_PRESSED:
						moveTiles(g, TRANS_DOWN);
						break;
					case RIGHT_PRESSED:
						moveTiles(g, TRANS_RIGHT);
						break;
					}
				}
				break;
			case STATE_NEW_GAME:
				updateHighScores();
				newGame(g);
				break;
			case STATE_NEW_TILE:
				newTile(g);
				break;
			case STATE_GAMEOVER:
				drawGameOver(g);
				setNewState(STATE_WAIT_KEY);
				break;
			case STATE_HELPVIEW:
				{
					int keyStates = getKeyStates();
					if (keyStates == FIRE_PRESSED)
					{
						switchFlag(FLAG_REQ_SHOW_HELP, true);
					}
					if (enablesFlag() && doFlagTask(g))
					{
						break mainloop;
					}
				}
				break;
			case STATE_WINNER:
				{
					int keyStates = getKeyStates();
					if (keyStates == FIRE_PRESSED)
					{
						setNewState(isGameOver() ? STATE_GAMEOVER : STATE_WAIT_KEY);
						redrawAll(g);
					}
					else if (enablesFlag() && doFlagTask(g))
					{
						break mainloop;
					}
				}
				break;
			case STATE_RESETSCORE:
				if (enablesFlag())
				{
					if (doFlagTask(g))
					{
						break mainloop;
					}
					continue mainloop;
				}
				{
					int keyStates = getKeyStates();
					if (keyStates == GAME_C_PRESSED)
					{
						clearHiscores();
					}
					else if (keyStates != GAME_D_PRESSED)
					{
						break;
					}
					restoreAll(g);
				}
				break;
			case STATE_SHOWSCORES:
				{
					int keyStates = getKeyStates();
					if (keyStates == FIRE_PRESSED)
					{
						switchFlag(FLAG_REQ_SHOW_SCORES, true);
					}
					if (enablesFlag() && doFlagTask(g))
					{
						break mainloop;
					}
				}
				break;
			case STATE_SHOWCAPTURES:
				{
					if (enablesFlag() && doFlagTask(g))
					{
						break mainloop;
					}
					int keyStates = getKeyStates();
					switch (keyStates)
					{
					case GAME_C_PRESSED:
						captureIndex = (captureIndex + captures.length - 1) % captures.length;
						break;
					case FIRE_PRESSED:
					case GAME_D_PRESSED:
						captureIndex = (captureIndex + 1) % captures.length;
						break;
					default:
						continue mainloop;
					}
					showCapture(g);
					waitController.await(200L); // reset keyStates?
				}
				break;
			}
		}
		restoreState();
		saveGameData();
	}

	public void requestEndGame()
	{
		switchFlag(FLAG_REQ_END_GAME, true);
	}
	
	public void requestNewGame()
	{
		switchFlag(FLAG_REQ_NEW_GAME, true);
	}
	
	public void requestShowHelp()
	{
		switchFlag(FLAG_REQ_SHOW_HELP, true);
	}
	
	public void requestResetScore()
	{
		switchFlag(FLAG_REQ_RESET_SCORE, true);
	}
	
	public void requestShowScores()
	{
		switchFlag(FLAG_REQ_SHOW_SCORES, true);
	}
	
	public void requestShowCaptures()
	{
		switchFlag(FLAG_REQ_SHOW_CAPTURES, true);
	}
}