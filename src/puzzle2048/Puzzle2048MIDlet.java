// Puzzle2048MIDlet
package puzzle2048;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

import puzzle2048.Puzzle2048Canvas;

public final class Puzzle2048MIDlet extends MIDlet implements CommandListener
{
	private Thread mainloop = null;
	
	private Puzzle2048Canvas game = null;
	
	private Command exitCommand    = null;
	private Command newgameCommand = null;
	private Command helpCommand    = null;
	private Command resetCommand   = null;
	private Command scoresCommand  = null;
	private Command capsCommand    = null;
	
	public Puzzle2048MIDlet()
	{
		super();
		
		game = new Puzzle2048Canvas(4, 4);
		
		exitCommand = new Command("EXIT", Command.EXIT, 1);		
		game.addCommand(exitCommand);

		newgameCommand = new Command("NEW", Command.SCREEN, 1);
		game.addCommand(newgameCommand);
		
		helpCommand = new Command("HELP", Command.SCREEN, 2);
		game.addCommand(helpCommand);
		
		resetCommand = new Command("RESET", Command.SCREEN, 3);
		game.addCommand(resetCommand);

		scoresCommand = new Command("SCORES", Command.SCREEN, 4);
		game.addCommand(scoresCommand);

		capsCommand = new Command("CAPS", Command.SCREEN, 4);
		game.addCommand(capsCommand);
		
		game.setCommandListener(this);
		
		Display.getDisplay(this).setCurrent(game);
	}
	
	private void requestEndGame()
	{
		game.requestEndGame();
		try
		{
			if (mainloop != null)
			{
				mainloop.join();
			}
		}
		catch (InterruptedException _)
		{
			// no code
		}		
	}
	
	protected void destroyApp(boolean unconditional)
			throws MIDletStateChangeException
	{
		requestEndGame();
	}
	
	protected void pauseApp()
	{
		// no code
	}
	
	protected void startApp()
			throws MIDletStateChangeException
	{
		if (mainloop == null)
		{
			mainloop = new Thread(game);
			mainloop.start();
		}
	}
	
	public void commandAction(Command cmd, Displayable disp)
	{
		if (cmd == exitCommand)
		{
			requestEndGame();
			notifyDestroyed();
		}
		else if (cmd == newgameCommand)
		{
			game.requestNewGame();
		}
		else if (cmd == helpCommand)
		{
			game.requestShowHelp();
		}
		else if (cmd == resetCommand)
		{
			game.requestResetScore();
		}
		else if (cmd == scoresCommand)
		{
			game.requestShowScores();
		}
		else if (cmd == capsCommand)
		{
			game.requestShowCaptures();
		}
	}
}