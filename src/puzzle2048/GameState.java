// GameState
package puzzle2048;

public final class GameState
{
	int state;
	int score;
	long scoreTime;
	boolean winner;
	int width;
	int height;
	int[][] field;
}
