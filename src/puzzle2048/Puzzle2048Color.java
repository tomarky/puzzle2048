// Puzzle2048Color
package puzzle2048;

public final class Puzzle2048Color
{
	public static final int BACK  = 0xFAF8EF;
	public static final int FIELD = 0xC7B697;
	
	public static final int SCOREBOARD      = 0xBBADA0;
	public static final int SCOREBOARD_FONT = 0xEADFD4;
	public static final int SCORE_FONT      = 0xFFFFFF;
	
	public static int getTileFontColor(int tile)
	{
		if (tile > 2)
		{
			return 0xF9F6F2;
		}
		else
		{
			return 0x776E65;
		}
	}
	
	public static int getTileColor(int tile)
	{
		switch (tile)
		{
		case  0: return 0xCDC1B4; /* empty */
		case  1: return 0xEEE4DA; /*    2  */
		case  2: return 0xEDE0C8; /*    4  */
		case  3: return 0xF2B179; /*    8  */
		case  4: return 0xF59563; /*   16  */
		case  5: return 0xF67C5F; /*   32  */
		case  6: return 0xF65E3B; /*   64  */
		case  7: return 0xEDCF72; /*  128  */
		case  8: return 0xEDCC61; /*  256  */
		case  9: return 0xEDC850; /*  512  */
		case 10: return 0xEDC53F; /* 1024  */
		case 11: return 0xEDC22E; /* 2048  */
		default: return 0x3C3A32; /* 4096~ */
		}
	}
}