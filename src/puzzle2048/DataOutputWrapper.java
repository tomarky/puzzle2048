// DataOutputWrapper
package puzzle2048;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public final class DataOutputWrapper
{
	private ByteArrayOutputStream baos = null;
	private DataOutputStream dos = null;

	public DataOutputWrapper(int size)
	{
		baos = new ByteArrayOutputStream(size);
		dos = new DataOutputStream(baos);
	}

	public DataOutputWrapper()
	{
		baos = new ByteArrayOutputStream();
		dos = new DataOutputStream(baos);
	}

	public void close()
	{
		if (dos != null)
		{
			try
			{
				dos.close();
			}
			catch (IOException _)
			{
				// no code
			}
			dos = null;
		}
		if (baos != null) // almost called baos.close() in dos.close()
		{
			try
			{
				baos.close();
			}
			catch (IOException _)
			{
				// no code
			}
			baos = null;
		}
	}

	public void writeInt(int value)
	{
		if (dos == null)
		{
			return;
		}
		try
		{
			dos.writeInt(value);
		}
		catch (IOException _)
		{
			// no code
		}
	}

	public void writeLong(long value)
	{
		if (dos == null)
		{
			return;
		}
		try
		{
			dos.writeLong(value);
		}
		catch (IOException _)
		{
			// no code
		}
	}

	public void writeBoolean(boolean value)
	{
		if (dos == null)
		{
			return;
		}
		try
		{
			dos.writeBoolean(value);
		}
		catch (IOException _)
		{
			// no code
		}
	}

	public byte[] getBytes()
	{
		if (baos == null)
		{
			return null;
		}
		return baos.toByteArray();
	}
}