// Puzzle2048RMS
package puzzle2048;

import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;

import puzzle2048.DataInputWrapper;
import puzzle2048.DataOutputWrapper;
import puzzle2048.GameState;

public final class Puzzle2048RMS
{	
	private static final int FORMAT_VERSION = 2;
	
	private RecordStore bestScoreRS  = null;
	private RecordStore gameStateRS  = null;
	private RecordStore highScoresRS = null;
	private RecordStore capturesRS   = null;
	
	public Puzzle2048RMS() 
	{
		try
		{
			bestScoreRS  = RecordStore.openRecordStore("puzzle2048.best", true);
			gameStateRS  = RecordStore.openRecordStore("puzzle2048.gamestate", true);			
			highScoresRS = RecordStore.openRecordStore("puzzle2048.highscores", true);			
			capturesRS   = RecordStore.openRecordStore("puzzle2048.captures", true);			
		}
		catch (RecordStoreException _)
		{
			// no code
		}
	}
	
	private RecordStore release(RecordStore rs)
	{
		if (rs != null)
		{
			try
			{
				rs.closeRecordStore();
			}
			catch (RecordStoreException _)
			{
				// no code;
			}
		}
		return null;
	}
	
	public void close()
	{
		bestScoreRS  = release(bestScoreRS);
		gameStateRS  = release(gameStateRS);
		highScoresRS = release(highScoresRS);
	}
	
	private void writeRecord(RecordStore rs, byte[] buf)
	{
		if (rs == null || buf == null)
		{
			return;
		}
		try
		{
			if (rs.getNumRecords() == 0)
			{
				rs.addRecord(buf, 0, buf.length);
			}
			else
			{
				rs.setRecord(1, buf, 0, buf.length);
			}
		}
		catch (RecordStoreException _)
		{
			// no code
		}
	}
	
	private byte[] getRecord(RecordStore rs, int num)
	{
		if (rs == null)
		{
			return null;
		}
		try
		{
			if (rs.getNumRecords() == 0)
			{
				return null;
			}
			return rs.getRecord(num);
		}
		catch (RecordStoreException _)
		{
			// no code
		}
		return null;
	}
	
	public void saveBestScore(int score)
	{
		if (bestScoreRS == null)
		{
			return;
		}
		byte[] buf = null;
		
		DataOutputWrapper dow = new DataOutputWrapper(4 * 2);
		dow.writeInt(FORMAT_VERSION);
		dow.writeInt(score);
		buf = dow.getBytes();
		dow.close();
		
		writeRecord(bestScoreRS, buf);
	}
	
	public int loadBestScore()
	{
		byte[] buf = getRecord(bestScoreRS, 1);
		if (buf == null)
		{
			return 0;
		}
		
		DataInputWrapper diw = new DataInputWrapper(buf);
		int _version = diw.readInt();
		int score = diw.readInt();
		diw.close();
		return score;
	}
	
	public void saveGameState(GameState gamestate)
	{
		if (gameStateRS == null)
		{
			return;
		}
		byte[] buf = null;
		
		int datasize = 1 + 4 * (5 + gamestate.width * gamestate.height);
		DataOutputWrapper dow = new DataOutputWrapper(datasize);
		dow.writeInt(FORMAT_VERSION);
		dow.writeInt(gamestate.state);
		dow.writeInt(gamestate.score);
		dow.writeLong(gamestate.scoreTime);
		dow.writeBoolean(gamestate.winner);
		dow.writeInt(gamestate.width);
		dow.writeInt(gamestate.height);
		for (int y = 0; y < gamestate.height; y++)
		{
			for (int x = 0; x < gamestate.width; x++)
			{
				dow.writeInt(gamestate.field[y][x]);
			}
		}
		buf = dow.getBytes();
		dow.close();
		
		writeRecord(gameStateRS, buf);
	}

	public GameState loadGameState(int[][] field, int width, int height)
	{
		byte[] buf = getRecord(gameStateRS, 1);
		if (buf == null)
		{
			return null;
		}
		
		GameState gamestate = new GameState();
		DataInputWrapper diw = new DataInputWrapper(buf);
		int version = diw.readInt();
		gamestate.state     = diw.readInt();
		gamestate.score     = diw.readInt();
		gamestate.scoreTime = version >= 2 ? diw.readLong() : 0;
		gamestate.winner    = diw.readBoolean();
		gamestate.width     = diw.readInt();
		gamestate.height    = diw.readInt();
		if (width != gamestate.width || height != gamestate.height)
		{
			gamestate.field = new int[gamestate.height][gamestate.width];
		}
		else
		{
			gamestate.field = field;
		}
		for (int y = 0; y < gamestate.height; y++)
		{
			for (int x = 0; x < gamestate.width; x++)
			{
				gamestate.field[y][x] = diw.readInt();
			}
		}
		diw.close();
		if (diw.hasError())
		{
			gamestate.field = null;
			gamestate = null;
			return null;
		}
		return gamestate;
	}
	
	public void saveHighScores(int[] highScores, long[] highScoreDates)
	{
		if (highScoresRS == null)
		{
			return;
		}
		byte[] buf = null;
		
		DataOutputWrapper dow = new DataOutputWrapper(4 * 2);
		dow.writeInt(FORMAT_VERSION);
		dow.writeInt(highScores.length);
		for (int i = 0; i < highScores.length; i++)
		{
			dow.writeInt(highScores[i]);
			dow.writeLong(highScoreDates[i]);
		}
		buf = dow.getBytes();
		dow.close();
		
		writeRecord(highScoresRS, buf);
	}
	
	public boolean loadHighScores(int[] highScores, long[] highScoreDates)
	{
		byte[] buf = getRecord(highScoresRS, 1);
		if (buf == null)
		{
			return false;
		}
		
		DataInputWrapper diw = new DataInputWrapper(buf);
		int _version = diw.readInt();
		int count = diw.readInt();
		if (highScores.length < count)
		{
			count = highScores.length;
		}
		for (int i = 0; i < count; i++)
		{
			highScores[i] = diw.readInt();
			highScoreDates[i] = diw.readLong();
		}
		diw.close();
		return true;
	}
	
		
	public void saveCaptures(int width, int height, int[][][] captures)
	{
		if (capturesRS == null)
		{
			return;
		}
		byte[] buf = null;
		
		DataOutputWrapper dow = new DataOutputWrapper(4 * 2);
		dow.writeInt(FORMAT_VERSION);
		dow.writeInt(width);
		dow.writeInt(height);
		dow.writeInt(captures.length);
		for (int i = 0; i < captures.length; i++)
		{
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					dow.writeInt(captures[i][y][x]);
				}
			}
		}
		buf = dow.getBytes();
		dow.close();
	
		writeRecord(capturesRS, buf);
	}
	
	public boolean loadCaptures(int width, int height, int[][][] captures)
	{
		byte[] buf = getRecord(capturesRS, 1);
		if (buf == null)
		{
			return false;
		}
		
		DataInputWrapper diw = new DataInputWrapper(buf);
		int _version = diw.readInt();
		int w        = diw.readInt();
		int h        = diw.readInt();
		int count    = diw.readInt();
		if (captures.length < count)
		{
			count = captures.length;
		}
		for (int i = 0; i < count; i++)
		{
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					int f = diw.readInt();
					if (x < width && y < height)
					{
						captures[i][y][x] = f;
					}
				}
			}
		}
		diw.close();
		return true;
	}
}