// WaitController
package puzzle2048;

public final class WaitController
{
	long limit = 0L, waitTime = 0L;

	public WaitController()
	{
		// no code
	}

	public void init(long newWaitTime)
	{
		waitTime = newWaitTime;
		limit = System.currentTimeMillis() + waitTime;
	}

	public void await()
	{
		long time;
		do
		{
			time = System.currentTimeMillis();
		} while (time < limit);
		limit += waitTime;
	}

	public void await(long waitTime)
	{
		init(waitTime);
		await();
	}
}